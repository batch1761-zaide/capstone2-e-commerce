//[SECTION] Dependencies and Modules
	const exp = require('express'); 
	const UserController = require('../controllers/userControllers');
	const Auth = require('../auth.js'); 

//[SECTION] Routing Component and Auth.js destructuring
	const router = exp.Router();
	const { AUTHENTICATE, AUTHORIZE } = Auth;

//[ROUTE] ---------------REGISTER A USER CONTROLLER
	router.post('/register', (req, res) => {
		UserController.REGISTER(req.body).then(resultFromController => 
			{ res.send(resultFromController); });
		});
 
//[ROUTE] --------------- RETRIVE ALL USERS CONTROLLER
		router.get("/allusers", AUTHENTICATE, AUTHORIZE, (req, res) => { 
			UserController.GETALLUSERS().then(resultFromController => 
				res.send(resultFromController))});

//[ROUTE] --------------- SET USER TO ADMIN (NO ADMIN ONLY ACCESS YET)
		router.put('/:userId', AUTHENTICATE, AUTHORIZE, (req, res) => {
			UserController.USERTOADMIN(req.params.userId, req.body).then(resultFromController => res.send(resultFromController))
		})

//[ROUTE] --------------- USER LOGIN - WITH AUTHENTICATION
		router.post('/login', (req, res) => {
			UserController.LOGIN(req.body).then(resultFromController => res.send(resultFromController));
		})


//[SECTION] Routes- DEL
//[SECTION] Expose Route System
	module.exports = router;




































