//[SECTION] Dependencies and Modules
	const exp = require('express');
	const Auth = require('../auth.js');
	const OrderController = require('../controllers/orderControllers');

//[SECTION] Destructuring of Auth functions from auth.js
	const { AUTHENTICATE, AUTHORIZE} = Auth;
	const router = exp.Router();

//[SECTION] ROUTES - POST
		//------------------Create an Order
		router.post('/createorder', AUTHENTICATE, OrderController.CREATEORDER);
		//------------------Add item to cart
		router.post('/additem', AUTHENTICATE, OrderController.addItemtoCart);
//[SECTION] ROUTES - GET
		//Retrieve all orders  (Admin only)
		router.get('/allorders', AUTHENTICATE, AUTHORIZE, (req, res) => {
			OrderController.GETALLORDERS().then(result => res.send(result));
		})
		//Get specific orders (By User)
		router.get('/getmyorders', AUTHENTICATE, OrderController.GETMYORDERS);

module.exports = router;
