//[SECTION] Dependencies and Modules
const express = require("express")
const ProductController = require("../controllers/productControllers")
const Auth = require('../auth.js');

//[SECTION] Routing Component
const router = express.Router();
const { AUTHENTICATE, AUTHORIZE} = Auth;

//[SECTION] ROUTES - POST
	//------------------Insert a Product
		router.post("/create", AUTHENTICATE, AUTHORIZE, (req, res) => {
		 ProductController.INSERTPRODUCT(req.body).then(resultFromController => res.send(resultFromController))});

//[SECTION] ROUTES - GET
	//------------------ Retrieve all Products
		router.get("/allproducts", (req, res) => {
			ProductController.GETALLPRODUCTS().then(
				resultFromController => 
				res.send(resultFromController))
		});

	//------------------ Retrieve a single Product
		router.get("/findproduct", (req, res) => {
			let productId = req.body.productId;

			if (productId === "") {
				res.send(`Please enter a Product ID to find!`);
			} else {
				ProductController.GETONEPRODUCT(productId).then(
					resultFromController => 
				res.send(resultFromController));
			}
		});
	//------------------ Retrieve all ACTIVE Products
		router.get("/activeproducts", (req, res) => {
			ProductController.GETACTIVEPRODUCTS().then(
				resultFromController => 
				res.send(resultFromController))
		});

//[SECTION] Routes- PUT
	//------------------ Update a Product Information
		router.put('/:productId', AUTHENTICATE, AUTHORIZE, (req, res) => {
			ProductController.UPDATEPRODUCT(req.params.productId, req.body).then(resultFromController => res.send(resultFromController))
		});

	//------------------ Archive a Product
		router.put('/archive/:archiveProductId', AUTHENTICATE, AUTHORIZE, (req, res) => {
			ProductController.ARCHIVEPRODUCT(req.params.archiveProductId, req.body).then(
				resultFromController => res.send(resultFromController))
	})

//[SECTION] Routes- DEL
//[SECTION] Expose Route System
module.exports = router;













