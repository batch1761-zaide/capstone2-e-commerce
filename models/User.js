//[SECTION] Dependencies and Modules
   const mongoose = require('mongoose');
   const { isEmail } = require('validator');
   
//[SECTION] Schema/Blueprint 
	const userSchema = new mongoose.Schema({
		email: {
			type: String,
			required: [true, 'Email is Required'],
			unique: true,
			//could be written as validate validate: [(val) = { }, 'Please enter a valid email'] but we can just install a validator dependency
			validate: [isEmail, 'Please enter a valid email']
		},
		password: {
			type: String,
			required: [true, 'Password is Required']
		},
		isAdmin: {
			type: Boolean,
			default: false
		}
	});

//[SECTION] Model
	module.exports = mongoose.model('User', userSchema); 
