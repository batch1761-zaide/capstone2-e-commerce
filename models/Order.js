//[SECTION] Dependencies and Modules
  const mongoose = require('mongoose');
  const Product = require('../models/Product');

//[SECTION] Schema/Blueprint --------- OLD SCHEMA
  // const orderSchema = new mongoose.Schema({
  //     userId: {
  //        type: String,
  //        required: [true, 'User ID is required for this order.']
  //     },
  //     product: [
  //        {
  //           productId: {
  //              type: String,
  //              required: [true, 'Product ID is Required'] 
  //           },
  //           quantity: {
  //              type: Number,
  //              required: [true, 'At least a quantity of one (1) is required'] 
  //           }
  //        }
  //     ],
  // 		totalAmount: {
  // 			type: Number
  // 		},
  // 		purchasedOn: {
  // 			type: Date,
  // 			default: new Date()
  // 		}
  // });

//[SECTION] Schema/Blueprint --------- NEW SCHEMA
const orderSchema = new mongoose.Schema({
   userId: {
      type: String,
      ref: 'User', 
       required: [true, 'User ID is Required'] 
    },
   products: [
      {
         productId: {
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'Product',
            required: [true, 'Product ID is Required']
          },
         productName: {
            type: String
         },
         quantity: {
            type: Number,
            default: 1
         }
      }
   ], 
   totalAmount: {
      type: Number,
   },
   purchasedOn: {
      type: Date,
      default: new Date()
   }
});



//[SECTION] Model
	module.exports = mongoose.model('Order', orderSchema);

