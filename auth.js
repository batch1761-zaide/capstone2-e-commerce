const jwt = require('jsonwebtoken');
const dotenv = require('dotenv'); 

dotenv.config();
const secret = process.env.ACCESS_TOKEN_SECRET;

//CREATE ACCESS TOKEN
module.exports.createAccessToken = (user) =>{

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data, secret, {})
}

//VERIFY IF THE USER IS AUTHENTICATED
module.exports.AUTHENTICATE = (req, res, next) => {

	let token = req.headers.authorization;

	if(typeof token === "undefined"){
		return res.send({ auth: "Failed. No Token found." });
	} else {
		
		token = token.slice(7, token.length)

		jwt.verify(token, secret, function(err, decodedToken) {

			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				})
			} else {
				
				req.user = decodedToken

				next();
			}
		})
	}
}


//VERIFY IF ADMIN IS LOGGED IN
module.exports.AUTHORIZE = (req, res, next) => {
	if(req.user.isAdmin) {
		next();
	}else {
		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
}































