//[SECTION] Dependencies and Modules
  const Product = require('../models/Product'); 

//[SECTION] Environment Variables Setup


//[SECTION] Functionalities [CREATE]
		//INSERT A PRODUCT
				module.exports.INSERTPRODUCT = (productData) => {
			    let pname = productData.name;
			    let desc = productData.description;
			    let price = productData.price;
			    let act = productData.isActive;

			    let newProduct = new Product({
			      name: pname,
			      description: desc,
			      price: price,
			      isActive : act
			    }); 

					return newProduct.save().then((product, err) => {
						if (product) {
							return product; 	
						} else {
							return ({Error_Message:"Failed to Insert Product."});
						}; 
					});
				};


//[SECTION] Functionalities [RETRIEVE]
		//RETRIEVE ALL PRODUCTS
				module.exports.GETALLPRODUCTS = () => {
					return Product.find({}).then(result => {
						return result;
					});
				};

		//RETRIEVE A SINGLE PRODUCT
				module.exports.GETONEPRODUCT = (getOneProduct) => {
					return Product.findById(getOneProduct).then((found,notFound) => {
						if(found) {
							return found;
						} else {
							return ({Message:"Product does not exist."});
						};
					});
				}
		//RETRIEVE A SINGLE PRODUCT
				module.exports.GETACTIVEPRODUCTS = () => {
					return Product.find({ isActive: true}).then((found,notFound) => {
						if(found) {
							return found;
						} else {
							return ({Message: "No Active Products."});
						};
					});
				}
//[SECTION] Functionalities [UPDATE]
		//UPDATE A PRODUCT INFORMATION
		module.exports.UPDATEPRODUCT = (productId, productData) => {
			let updatedProduct = {
				name: productData.New_Product_Name,
				description: productData.Description,
				price: productData.Price,
				isActive: productData.Is_Available
			}

			//findByIdAndUpdate(document Id, updatesTobeApplied)
			return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
				if(error){
					return ({Error_Message: "Failed to Update this Product."});
				} else {
					return updatedProduct;
				}
			}).catch(error => error)
		}
		//ARCHIVE A PRODUCT
		module.exports.ARCHIVEPRODUCT = (productId, productData) => {
			let archiveProduct = {
				name: productData.Product_Name,
				isActive: false
			}
			return Product.findByIdAndUpdate(productId, archiveProduct).then((product, error) => {
				if(error){
					return false;
				} else {
					return  `"Product "${productData.Product_Name}" is now archived.

										- 🅲🆄🆁🆁🅴🅽🆃🅻🆈 🅾🆄🆃 🅾🅵 🆂🆃🅾🅲🅺
									`;
				}
			}).catch(error => error)
		}

//[SECTION] Functionalities [DELETE]


