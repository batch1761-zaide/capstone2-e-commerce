//[SECTION] Dependencies and Modules
  const User = require('../models/User');
  const bcrypt = require('bcrypt');
  const dotenv = require('dotenv');
  const jwt = require('jsonwebtoken');
  const auth = require('../auth.js');

//[SECTION] Environment Variables Setup
    dotenv.config();
    const salt = Number(process.env.SALT);


//[SECTION] MIDDLEWARE --------------- AUTHENTICATE FUNCTION
	function authenticateToken (req, res, next) {
		const authHeader = req.headers['authorization']
		const token = authHeader && authHeader.split(' ')[1]
		if(token == null) return res.sendStatus(401)

		jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, email) => {
			if(err) return res.sendStatus(403)
				req.email = email
				next()
		})
	}


//[SECTION] Functionalities [CREATE A USER]

	module.exports.REGISTER = (userData) => {
        let email = userData.email;
        let passW = userData.password;
        let isAdmin = userData.isAdmin;
      
        let newUser = new User({
        	email: email,
        	password: bcrypt.hashSync(passW, salt),
        	isAdmin: isAdmin
        }); 
       
		return newUser.save().then((user, err) => {
			
			if (user) {
				return user; 	
			} else {
				return 'Failed to Register User'; 
			}; 
		}); 
	};


//[SECTION] Functionalities [USER LOGIN]
module.exports.LOGIN = (userData) => {
    return User.findOne({ email: userData.email }).then(result => {
        if(result) {
            const isPasswordCorrect = bcrypt.compareSync(userData.password, result.password)

            if(isPasswordCorrect){
            return { accessToken: auth.createAccessToken(result.toObject()) }
            }else {
               return ({Error_Message: "Password is incorrect"});
            }
            } 
            else {
              return ({Error_Message: "User does not exist"});
            }
        })
}


//[SECTION] Functionalities [RETRIEVE]
//  ------------------------------ Retrieve all users (Only an ADMIN can do this)
	module.exports.GETALLUSERS = () => {
		return User.find({}).then(result => { return result;	
		});	
	};

//[SECTION] Functionalities [UPDATE]
// ------------------------------ Set user to Admin (Only an ADMIN can do this)
module.exports.USERTOADMIN = (userId, userData) => {
	//specify the fields/properties of the dcument to be updated
	let newAdmin = {
		isAdmin: true
	}
	return User.findByIdAndUpdate(userId, newAdmin).then((user, error) => {
		if(error){
			return false;
		} else {
			return  `"Sᴜᴄᴄᴇssғᴜʟʟʏ ᴜᴘᴅᴀᴛᴇᴅ Usᴇʀ "${userData.email}"" ᴛᴏ Aᴅᴍɪɴ.

									🅲🅾🅽🅶🆁🅰🆃🆄🅻🅰🆃🅸🅾🅽🆂❗

									~Wɪᴛʜ ɢʀᴇᴀᴛ ᴘᴏᴡᴇʀ ᴄᴏᴍᴇs ɢʀᴇᴀᴛ ʀᴇsᴘᴏɴsɪʙɪʟɪᴛʏ - Uɴᴄʟᴇ Bᴇɴ (Sᴘɪᴅᴇʀᴍᴀɴ)
							`;
		}
	}).catch(error => error)
}

//[SECTION] Functionalities [DELETE]
