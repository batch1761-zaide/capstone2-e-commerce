//[SECTION] Dependencies and Modules
	const User = require('../models/User');
	const Product = require('../models/Product');
	const Order = require('../models/Order');
	const mongoose = require('mongoose'); 

//[SECTION] Create Order
	module.exports.CREATEORDER = async (req, res) => {
		let grandTotal = 0;

		if(req.user.isAdmin) {
			res.send({Warning: "You are creating an order using your Admin Access. Please use your personal account."});
		}

		const newOrder = new Order({
			userId: req.user.id,
			products: [],
			totalAmount: 0
		})

		for(let i = 0; i<req.body.length; i++) {
			let isProductAvailable = await Product.findById(req.body[i].productId)
				.then( product => {
				if(product) {
					if(product.isActive) {
						let newProduct = {
							productId: req.body[i].productId,
							productName: product.name,
							quantity: req.body[i].quantity
						}

					newOrder.totalAmount += (req.body[i].quantity * product.price)
					newOrder.products.push(newProduct)
					} else {
						res.send(`Product is not active`);
					}
				} else {
					return res.send(`Not existing product`);
				}
			})
		}

		newOrder.save().then(result => {
			return res.send(result)
		}).catch(error => res.send(error))

}







































//[SECTION] Get all orders (Admin only)
module.exports.GETALLORDERS = () => {
	return Order.find({}).then(result => {
		return result;
	}).catch(error => error)
	
}
//[SECTION] Get specific orders (By User)
module.exports.GETMYORDERS = (req, res) => {
	Order.find({ 'userId' : req.user.id })
	.then(result =>
		{
			res.send(result)
	})
	.catch(error => err.message)
}

//[SECTION] Add Item to Order
module.exports.addItemtoCart = () => {


}



